import 'dart:convert';

import 'package:flutter/services.dart';

class DiscoverReponsitory {
  Future<List<String>> getBrowserAll() async {
    List<String> browsersAll = [];
    final fileString = await rootBundle.loadString('assets/jsons/browser.json');
    final Map<String, dynamic> jsonMap = json.decode(fileString);
    final list = jsonMap['data'];
    list?.forEach((item) => {browsersAll.add(item)});
    return browsersAll;
  }
}
