import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_event.dart';
import 'package:mecar_interview/blocs/auth/auth_state.dart';
import 'package:mecar_interview/repositories/user_repository.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final UserRepository _userRepository;

  AuthBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(AuthInitial());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthStarted) {
      yield* _authStartedToState();
    } else if (event is AuthLoggedIn) {
      yield* _authLoggedInToState();
    } else if (event is AuthLoggedOut) {
      yield* _authLoggedOutInToState();
    }
  }

  Stream<AuthState> _authLoggedOutInToState() async* {
    yield AuthFailure();
    _userRepository.signOut();
  }

  Stream<AuthState> _authStartedToState() async* {
    final isSignedIn = _userRepository.isSignedIn();
    if (isSignedIn) {
      final firebaseUser = _userRepository.getUser();
      yield AuthSuccess(firebaseUser);
    } else {
      yield AuthFailure();
    }
  }

  Stream<AuthState> _authLoggedInToState() async* {
    yield AuthSuccess(_userRepository.getUser());
  }
}
