import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

abstract class AuthState extends Equatable {
  AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthSuccess extends AuthState {
  final User firebaseUser;

  AuthSuccess(this.firebaseUser);

  @override
  List<Object> get props => [firebaseUser];
}

class AuthFailure extends AuthState {}
