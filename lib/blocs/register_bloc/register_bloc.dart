import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/blocs/register_bloc/register_event.dart';
import 'package:mecar_interview/blocs/register_bloc/register_state.dart';
import 'package:mecar_interview/repositories/user_repository.dart';
import 'package:mecar_interview/utils/validate.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(RegisterState.initial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterEmailChange) {
      yield* _mapLoginEmailChangeToState(event.email);
    } else if (event is RegisterPasswordChange) {
      yield* _mapLoginPasswordChangeToState(event.password);
    } else if (event is RegisterConfirmPasswordChange) {
      yield* _mapLoginConfirmPasswordChangeToState(
          event.password, event.confirmPassword);
    } else if (event is RegisterSubmitted) {
      yield* _mapLoginWithCredentialsPressedToState(
          email: event.email, password: event.password);
    }
  }

  Stream<RegisterState> _mapLoginEmailChangeToState(String email) async* {
    yield state.update(isEmailValid: Validators.isValidEmail(email));
  }

  Stream<RegisterState> _mapLoginPasswordChangeToState(String password) async* {
    yield state.update(isPasswordValid: Validators.isValidPassword(password));
  }

  Stream<RegisterState> _mapLoginConfirmPasswordChangeToState(
      String password, String confirmPassword) async* {
    final isPasswordMapped = password == confirmPassword;
    yield state.update(
        isconfirmPasswordValid:
            (Validators.isValidPassword(confirmPassword) && isPasswordMapped));
  }

  Stream<RegisterState> _mapLoginWithCredentialsPressedToState(
      {String email, String password}) async* {
    yield RegisterState.loading();
    try {
      await _userRepository.signUp(email, password);
      yield RegisterState.success();
    } catch (_) {
      yield RegisterState.failure();
    }
  }
}
