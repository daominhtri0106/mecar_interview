import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class DiscoverState extends Equatable {
  List<String> browsersAll;
  DiscoverState({this.browsersAll});

  @override
  List<Object> get props => [];
}

class DiscoverInitialState extends DiscoverState {}

class DiscoverLoadingState extends DiscoverState {}

class DiscoverLoadedState extends DiscoverState {
  final List<String> browserAll;

  DiscoverLoadedState({@required this.browserAll});

  @override
  List<Object> get props => [browserAll];
}

class DiscoverErrorState extends DiscoverState {
  String message;
  DiscoverErrorState({@required this.message});
}
