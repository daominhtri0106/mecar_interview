import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/repositories/discover_repository.dart';

import 'discover_event.dart';
import 'discover_state.dart';

class DiscoverBloc extends Bloc<DiscoverEvent, DiscoverState> {
  final DiscoverReponsitory _discoverReponsitory;

  DiscoverBloc({DiscoverReponsitory discoverReponsitory})
      : _discoverReponsitory = discoverReponsitory,
        super(DiscoverInitialState());

  @override
  Stream<DiscoverState> mapEventToState(DiscoverEvent event) async* {
    if (event is DiscoverStarted) {
      yield* _getDiscoverStartedToState();
    }
  }

  Stream<DiscoverState> _getDiscoverStartedToState() async* {
    yield DiscoverLoadingState();
    try {
      final discovers = await _discoverReponsitory.getBrowserAll();
      yield DiscoverLoadedState(browserAll: discovers);
    } catch (e) {
      yield DiscoverErrorState(message: e.toString());
    }
  }

  Stream<DiscoverState> _discoverDefault() async* {
    yield DiscoverInitialState();
  }
}
