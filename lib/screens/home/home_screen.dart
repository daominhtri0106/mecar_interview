import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/blocs/discover_bloc/discover_bloc.dart';
import 'package:mecar_interview/blocs/discover_bloc/discover_event.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/repositories/discover_repository.dart';
import 'package:mecar_interview/screens/home/bottom_navigation_bar_custom.dart';
import 'package:mecar_interview/screens/home/browser_all.dart';
import 'package:mecar_interview/screens/home/what_new_today.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final DiscoverReponsitory discoverRepository = DiscoverReponsitory();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DiscoverBloc>(
      create: (context) => DiscoverBloc(discoverReponsitory: discoverRepository)
        ..add(DiscoverStarted()),
      child: Scaffold(
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 56.0),
                  Text(
                    AppLocalizations.of(context).translate('discover_title'),
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  SizedBox(height: 20.0),
                  WhatNewsToDay(),
                  SizedBox(height: 32.0),
                  BrowserAll(),
                  SizedBox(height: 32.0),
                ],
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigationBarCustom()),
    );
  }
}
