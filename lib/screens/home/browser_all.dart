import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:mecar_interview/blocs/discover_bloc/discover_bloc.dart';
import 'package:mecar_interview/blocs/discover_bloc/discover_state.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/widgets/outline_button_custom.dart';

class BrowserAll extends StatelessWidget {
  const BrowserAll({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppLocalizations.of(context).translate('discover_browser_title'),
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                  fontWeight: FontWeight.bold,
                ),
          ),
          BlocListener<DiscoverBloc, DiscoverState>(
            listener: (BuildContext context, state) {
              if (state is DiscoverErrorState) {
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(state.message),
                        ],
                      ),
                      backgroundColor: Color(0xffffae88),
                    ),
                  );
              }
            },
            child: BlocBuilder<DiscoverBloc, DiscoverState>(
              builder: (BuildContext context, DiscoverState state) {
                if (state is DiscoverInitialState) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                    ),
                  );
                } else if (state is DiscoverLoadingState) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
                    ),
                  );
                } else if (state is DiscoverLoadedState) {
                  return StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    crossAxisCount: 2,
                    itemCount: state.browserAll.length,
                    itemBuilder: (BuildContext context, int index) => Container(
                      child: Image.asset(
                        state.browserAll[index],
                        fit: BoxFit.cover,
                      ),
                    ),
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(1),
                    mainAxisSpacing: 8.0,
                    crossAxisSpacing: 8.0,
                  );
                }
              },
            ),
          ),
          SizedBox(height: 16.0),
          SizedBox(
            width: double.infinity,
            child: OutlineButtonCustom(
              text: AppLocalizations.of(context)
                  .translate('discover_button_see_more'),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
