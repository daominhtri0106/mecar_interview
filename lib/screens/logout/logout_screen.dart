import 'package:flutter/material.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/repositories/user_repository.dart';
import 'package:mecar_interview/router/router.dart';
import 'package:mecar_interview/widgets/outline_button_custom.dart';
import 'package:mecar_interview/widgets/raised_button_custom.dart';

class LogoutScreen extends StatefulWidget {
  UserRepository userRepository;

  LogoutScreen({Key key, this.userRepository}) : super(key: key);

  @override
  _LogoutScreenState createState() => _LogoutScreenState();
}

class _LogoutScreenState extends State<LogoutScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: _buildBackground(),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
            child: _buildBottomButton(context),
          )
        ],
      ),
    );
  }

  Widget _buildBackground() {
    return Stack(
      fit: StackFit.expand,
      children: [
        Image.asset(
          'assets/images/logout.jpg',
          fit: BoxFit.cover,
        ),
        Align(
          alignment: Alignment.center,
          child: Image.asset('assets/images/union.png'),
        ),
        Positioned(
          bottom: 20.0,
          left: 15.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                'assets/images/pawel_czerwinski.png',
                width: 28,
                height: 28,
              ),
              SizedBox(width: 8.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Pawel Czerwinski',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13.0,
                    ),
                  ),
                  Text(
                    '@pawel_czerwinski',
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.8),
                      fontSize: 11,
                    ),
                  )
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildBottomButton(BuildContext context) {
    final isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: OutlineButtonCustom(
            text: AppLocalizations.of(context).translate('button_login'),
            textColors: isDark ? Colors.white : Colors.black,
            backgroundColor: isDark ? Colors.black : Colors.white,
            borderColor: isDark ? Colors.white : Colors.black,
            onPressed: () => {
              Routes.fluroRouter.navigateTo(
                context,
                Routes.login,
                routeSettings: RouteSettings(
                  arguments: widget.userRepository,
                ),
              )
            },
          ),
        ),
        SizedBox(width: 8.0),
        Expanded(
          child: RaisedButtonCustom(
            text: AppLocalizations.of(context).translate('button_register'),
            textColors: isDark ? Colors.black : Colors.white,
            backgroundColor: isDark ? Colors.white : Colors.black,
            onPressed: () => {
              Routes.fluroRouter.navigateTo(
                context,
                Routes.register,
                routeSettings: RouteSettings(
                  arguments: widget.userRepository,
                ),
              )
            },
          ),
        )
      ],
    );
  }
}
