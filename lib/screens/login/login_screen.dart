import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_event.dart';
import 'package:mecar_interview/blocs/login_bloc/login_bloc.dart';
import 'package:mecar_interview/blocs/login_bloc/login_event.dart';
import 'package:mecar_interview/blocs/login_bloc/login_state.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/repositories/user_repository.dart';
import 'package:mecar_interview/router/router.dart';
import 'package:mecar_interview/widgets/raised_button_custom.dart';
import 'package:mecar_interview/widgets/snackbar_custom.dart';
import 'package:mecar_interview/widgets/text_form_feild_custom.dart';

class LoginScreen extends StatefulWidget {
  UserRepository userRepository;
  LoginScreen({Key key, this.userRepository}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  LoginBloc _loginBloc;

  bool isButtonEnabled(LoginState state) {
    return state.isFormValid &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _emailController.addListener(() {
      _onEmailChange();
    });
    _passwordController.addListener(() {
      _onPasswordChange();
    });
  }

  @override
  Widget build(BuildContext context) {
    final isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;
    return GestureDetector(
      onTap: () {
        _emailFocus.unfocus();
        _passwordFocus.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: GestureDetector(
            onTap: () {
              Routes.fluroRouter.pop(context);
            },
            child: Icon(
              CupertinoIcons.arrow_turn_up_left,
              size: 20,
              color: isDark ? Colors.white : Colors.black,
            ),
          ),
        ),
        body: BlocProvider<LoginBloc>(
          create: (BuildContext context) =>
              LoginBloc(userRepository: widget.userRepository),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: BlocListener<LoginBloc, LoginState>(
                listener: (context, state) {
                  if (state.isFailure) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_login_failure'),
                    );
                  }

                  if (state.isSubmitting) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_login'),
                    );
                  }
                  if (state.isSuccess) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_login_success'),
                    );
                    Routes.fluroRouter.pop(context);
                    BlocProvider.of<AuthBloc>(context).add(
                      AuthLoggedIn(),
                    );
                  }
                },
                child: BlocBuilder<LoginBloc, LoginState>(
                  builder: (context, state) {
                    _loginBloc = BlocProvider.of<LoginBloc>(context);

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            AppLocalizations.of(context)
                                .translate('login_title'),
                            style: Theme.of(context).textTheme.headline4),
                        SizedBox(
                          height: 36.0,
                        ),
                        TextFormFeildCustom(
                          labelText: AppLocalizations.of(context)
                              .translate('login_email_input_placeholder'),
                          controller: _emailController,
                          focusNode: _emailFocus,
                          obscureText: false,
                          validator: (_) {
                            return !state.isEmailValid
                                ? AppLocalizations.of(context)
                                    .translate('common_invalid_email')
                                : null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        TextFormFeildCustom(
                          labelText: AppLocalizations.of(context)
                              .translate('login_password_input_placeholder'),
                          controller: _passwordController,
                          focusNode: _passwordFocus,
                          obscureText: true,
                          validator: (_) {
                            return !state.isPasswordValid
                                ? AppLocalizations.of(context)
                                    .translate('common_invalid_password')
                                : null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        SizedBox(
                          width: double.infinity,
                          child: RaisedButtonCustom(
                            text: AppLocalizations.of(context)
                                .translate('button_login'),
                            disabled: !isButtonEnabled(state),
                            textColors: isDark ? Colors.black : Colors.white,
                            backgroundColor:
                                isDark ? Colors.white : Colors.black,
                            onPressed: () => {
                              if (isButtonEnabled(state)) {_onFormSubmitted()}
                            },
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
  }

  void _onEmailChange() {
    _loginBloc.add(LoginEmailChange(email: _emailController.text));
  }

  void _onPasswordChange() {
    _loginBloc.add(LoginPasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _loginBloc.add(LoginSubmitted(
        email: _emailController.text, password: _passwordController.text));
  }
}
