import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_event.dart';
import 'package:mecar_interview/blocs/register_bloc/register_bloc.dart';
import 'package:mecar_interview/blocs/register_bloc/register_event.dart';
import 'package:mecar_interview/blocs/register_bloc/register_state.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/repositories/user_repository.dart';
import 'package:mecar_interview/router/router.dart';
import 'package:mecar_interview/widgets/raised_button_custom.dart';
import 'package:mecar_interview/widgets/snackbar_custom.dart';
import 'package:mecar_interview/widgets/text_form_feild_custom.dart';

class RegisterScreen extends StatefulWidget {
  UserRepository userRepository;
  RegisterScreen({Key key, this.userRepository}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();

  RegisterBloc _registerBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  bool isButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _emailController.addListener(() {
      _onEmailChange();
    });
    _passwordController.addListener(() {
      _onPasswordChange();
      _onConfirmPasswordChange();
    });

    _confirmPasswordController.addListener(() {
      _onConfirmPasswordChange();
    });
  }

  @override
  Widget build(BuildContext context) {
    final isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;
    return GestureDetector(
      onTap: () {
        _emailFocus.unfocus();
        _passwordFocus.unfocus();
        _confirmPasswordFocus.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          leading: GestureDetector(
            onTap: () {
              Routes.fluroRouter.pop(context);
            },
            child: Icon(
              CupertinoIcons.arrow_turn_up_left,
              size: 20,
              color: isDark ? Colors.white : Colors.black,
            ),
          ),
        ),
        body: BlocProvider<RegisterBloc>(
          create: (BuildContext context) =>
              RegisterBloc(userRepository: widget.userRepository),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: BlocListener<RegisterBloc, RegisterState>(
                listener: (context, state) {
                  if (state.isFailure) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_register_failure'),
                    );
                  }

                  if (state.isSubmitting) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_register'),
                    );
                  }
                  if (state.isSuccess) {
                    SnackBarCustom.showSnackBar(
                      context,
                      message: AppLocalizations.of(context)
                          .translate('common_status_register_success'),
                    );
                    Routes.fluroRouter.pop(context);
                    BlocProvider.of<AuthBloc>(context).add(
                      AuthLoggedIn(),
                    );
                  }
                },
                child: BlocBuilder<RegisterBloc, RegisterState>(
                  builder: (context, state) {
                    _registerBloc = BlocProvider.of<RegisterBloc>(context);

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            AppLocalizations.of(context)
                                .translate('register_title'),
                            style: Theme.of(context).textTheme.headline4),
                        SizedBox(
                          height: 36.0,
                        ),
                        TextFormFeildCustom(
                          labelText: AppLocalizations.of(context).translate(
                            'register_email_input_placeholder',
                          ),
                          controller: _emailController,
                          obscureText: false,
                          focusNode: _emailFocus,
                          validator: (_) {
                            return !state.isEmailValid
                                ? AppLocalizations.of(context).translate(
                                    'common_invalid_email',
                                  )
                                : null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        TextFormFeildCustom(
                          labelText: AppLocalizations.of(context).translate(
                            'register_password_input_placeholder',
                          ),
                          controller: _passwordController,
                          focusNode: _passwordFocus,
                          obscureText: true,
                          validator: (_) {
                            return !state.isPasswordValid
                                ? AppLocalizations.of(context).translate(
                                    'common_invalid_password',
                                  )
                                : null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        TextFormFeildCustom(
                          labelText: AppLocalizations.of(context).translate(
                            'register_confirm_password_input_placeholder',
                          ),
                          controller: _confirmPasswordController,
                          focusNode: _confirmPasswordFocus,
                          obscureText: true,
                          validator: (_) {
                            return !state.isconfirmPasswordValid
                                ? AppLocalizations.of(context).translate(
                                    'common_invalid_confirm_password',
                                  )
                                : null;
                          },
                        ),
                        SizedBox(height: 16.0),
                        SizedBox(
                          width: double.infinity,
                          child: RaisedButtonCustom(
                            text: AppLocalizations.of(context)
                                .translate('register_button_signup'),
                            disabled: !isButtonEnabled(state),
                            textColors: isDark ? Colors.black : Colors.white,
                            backgroundColor:
                                isDark ? Colors.white : Colors.black,
                            onPressed: () => {
                              if (isButtonEnabled(state)) {_onFormSubmitted()}
                            },
                          ),
                        ),
                        SizedBox(height: 32.0),
                        RichText(
                          text: TextSpan(
                            text: AppLocalizations.of(context)
                                .translate('register_term_service_first'),
                            style: Theme.of(context).textTheme.caption,
                            children: <TextSpan>[
                              TextSpan(
                                text: AppLocalizations.of(context)
                                    .translate('register_term_service_last'),
                                style: Theme.of(context)
                                    .textTheme
                                    .caption
                                    .copyWith(
                                      decoration: TextDecoration.underline,
                                    ),
                              )
                            ],
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
    _confirmPasswordFocus.dispose();
    super.dispose();
  }

  void _onEmailChange() {
    _registerBloc.add(RegisterEmailChange(email: _emailController.text));
  }

  void _onPasswordChange() {
    _registerBloc
        .add(RegisterPasswordChange(password: _passwordController.text));
  }

  void _onConfirmPasswordChange() {
    _registerBloc.add(RegisterConfirmPasswordChange(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onFormSubmitted() {
    _registerBloc.add(RegisterSubmitted(
        email: _emailController.text, password: _passwordController.text));
  }
}
