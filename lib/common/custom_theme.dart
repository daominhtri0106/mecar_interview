import 'package:flutter/material.dart';

class CustomTheme {
  ThemeData darkTheme(BuildContext context) {
    return ThemeData.dark().copyWith(
      primaryColor: Colors.black,
      canvasColor: Colors.white,
      scaffoldBackgroundColor: Colors.black,
      brightness: Brightness.dark,
      textTheme: Theme.of(context).textTheme.copyWith(
            bodyText2: TextStyle(color: Colors.white),
            headline4: TextStyle(color: Colors.white),
            caption: TextStyle(color: Colors.white),
          ),
    );
  }

  ThemeData lightTheme(BuildContext context) {
    return ThemeData.light().copyWith(
      primaryColor: Colors.black,
      canvasColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      brightness: Brightness.light,
      textTheme: Theme.of(context).textTheme.copyWith(
            bodyText2: TextStyle(color: Colors.black),
            headline4: TextStyle(color: Colors.black),
            caption: TextStyle(color: Colors.black),
          ),
    );
  }
}
