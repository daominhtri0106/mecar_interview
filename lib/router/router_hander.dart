import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:mecar_interview/screens/home/home_screen.dart';
import 'package:mecar_interview/screens/login/login_screen.dart';
import 'package:mecar_interview/screens/logout/logout_screen.dart';
import 'package:mecar_interview/screens/register/register_screen.dart';

var loginHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  final args = context.settings.arguments;
  return LoginScreen(userRepository: args);
});

var logoutHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return LogoutScreen();
});

var homeHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return HomeScreen();
});

var registerHandler = Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  final args = context.settings.arguments;

  return RegisterScreen(
    userRepository: args,
  );
});
