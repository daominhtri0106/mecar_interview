import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:mecar_interview/router/router_hander.dart';

class Routes {
  static FluroRouter fluroRouter;
  Routes();

  static String login = '/login';
  static String home = '/home';
  static String logout = '/logout';
  static String register = '/register';

  static void configureRoutes(FluroRouter fluroRouter) {
    fluroRouter.notFoundHandler = Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
      return;
    });

    fluroRouter.define(login, handler: loginHandler);
    fluroRouter.define(home, handler: homeHandler);
    fluroRouter.define(logout, handler: logoutHandler);
    fluroRouter.define(register, handler: registerHandler);
  }
}
