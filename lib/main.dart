import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mecar_interview/blocs/auth/auth_bloc.dart';
import 'package:mecar_interview/blocs/auth/auth_event.dart';
import 'package:mecar_interview/blocs/auth/auth_state.dart';
import 'package:mecar_interview/common/app_localizations.dart';
import 'package:mecar_interview/common/custom_theme.dart';
import 'package:mecar_interview/repositories/user_repository.dart';
import 'package:mecar_interview/router/router.dart';
import 'package:mecar_interview/screens/home/home_screen.dart';
import 'package:mecar_interview/screens/logout/logout_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  FirebaseAnalytics analytics = FirebaseAnalytics();
  final UserRepository userRepository = UserRepository();

  MyApp() {
    final router = FluroRouter();
    Routes.configureRoutes(router);
    Routes.fluroRouter = router;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          AuthBloc(userRepository: userRepository)..add(AuthStarted()),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: CustomTheme().lightTheme(context),
        darkTheme: CustomTheme().darkTheme(context),
        onGenerateRoute: Routes.fluroRouter.generator,
        navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: analytics),
        ],
        supportedLocales: [
          Locale('en', 'US'),
          Locale('vi', 'VN'),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        localeResolutionCallback: (
          Locale locale,
          Iterable<Locale> supportedLocales,
        ) {
          for (var s in supportedLocales) {
            if (s.languageCode == locale.languageCode &&
                s.countryCode == locale.countryCode) {
              return locale;
            }
          }
          return supportedLocales.first;
        },
        home: BlocBuilder<AuthBloc, AuthState>(
          builder: (BuildContext context, AuthState state) {
            if (state is AuthFailure) {
              return LogoutScreen(userRepository: userRepository);
            }

            if (state is AuthSuccess) {
              return HomeScreen();
            }

            return Scaffold(
              appBar: AppBar(),
              body: Container(
                child: Center(child: Text("Loading")),
              ),
            );
          },
        ),
      ),
    );
  }
}
