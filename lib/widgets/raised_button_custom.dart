import 'package:flutter/material.dart';

class RaisedButtonCustom extends StatelessWidget {
  String text;
  Color backgroundColor;
  Color textColors;
  BorderRadiusGeometry borderRadius;
  bool disabled;

  Function onPressed;

  RaisedButtonCustom({
    Key key,
    this.text,
    this.onPressed,
    this.backgroundColor = Colors.black,
    this.textColors = Colors.white,
    this.disabled = false,
    this.borderRadius = const BorderRadius.all(Radius.circular(6)),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        onPrimary: Colors.black,
        primary: Colors.white,
        minimumSize: Size(88, 52),
        padding: EdgeInsets.symmetric(horizontal: 16),
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius,
        ),
      ).copyWith(
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return backgroundColor;
          },
        ),
      ),
      onPressed: disabled ? null : () => onPressed(),
      child: Text(
        text,
        style: TextStyle(
          color: textColors,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
