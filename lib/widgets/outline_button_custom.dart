import 'package:flutter/material.dart';

class OutlineButtonCustom extends StatelessWidget {
  String text;
  Color borderColor;
  Color backgroundColor;
  Color textColors;
  BorderRadiusGeometry borderRadius;
  bool disabled;
  Function onPressed;

  OutlineButtonCustom({
    Key key,
    this.text,
    this.onPressed,
    this.borderColor = Colors.black,
    this.textColors = Colors.black,
    this.backgroundColor = Colors.white,
    this.disabled = false,
    this.borderRadius = const BorderRadius.all(
      Radius.circular(6),
    ),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(
        primary: Colors.black87,
        minimumSize: Size(88, 52),
        padding: EdgeInsets.symmetric(horizontal: 16),
        shape: RoundedRectangleBorder(
          borderRadius: borderRadius,
        ),
      ).copyWith(
        side: MaterialStateProperty.resolveWith<BorderSide>(
          (Set<MaterialState> states) {
            return BorderSide(
              color: borderColor,
              width: 2,
            );
          },
        ),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (Set<MaterialState> states) {
            return backgroundColor;
          },
        ),
      ),
      onPressed: disabled ? null : () => onPressed(),
      child: Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: textColors,
        ),
      ),
    );
  }
}
