import 'package:flutter/material.dart';

class SnackBarCustom {
  static showSnackBar(BuildContext context, {String message}) {
    return ScaffoldMessenger.of(context)
      ..removeCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                message,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              Icon(Icons.error),
            ],
          ),
          backgroundColor: Colors.green,
        ),
      );
  }
}
