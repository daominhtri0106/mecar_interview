import 'package:flutter/material.dart';

class TextFormFeildCustom extends StatelessWidget {
  String labelText;
  bool obscureText;
  TextEditingController controller;
  String Function(String) validator;
  FocusNode focusNode;

  TextFormFeildCustom(
      {Key key,
      this.labelText,
      this.controller,
      this.validator,
      this.obscureText = false,
      this.focusNode})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final isDark = MediaQuery.of(context).platformBrightness == Brightness.dark;

    return TextFormField(
      style: TextStyle(color: isDark ? Colors.white : Colors.black),
      focusNode: focusNode,
      decoration: InputDecoration(
        labelText: labelText,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(3),
          borderSide: BorderSide(
            color: Colors.blue,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(3),
          borderSide: BorderSide(
            color: isDark ? Colors.white : Colors.black,
            width: 2.0,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 2.0),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red, width: 2.0),
        ),
      ),
      controller: controller,
      keyboardType: TextInputType.visiblePassword,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      obscureText: obscureText,
      validator: validator,
    );
  }
}
